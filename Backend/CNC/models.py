from django.db import models


class State(models.Model):
    temperature = models.FloatField(null=True)
    humidity = models.FloatField(null=True)
    pitch = models.FloatField(null=True)
    roll = models.FloatField(null=True)
    yaw = models.FloatField(null=True)
    pixels = models.TextField(null=True)

