from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.core.cache import cache
from django.views.decorators.http import require_http_methods
from django.views.decorators.http import require_POST
import json
from pdb import set_trace as bp
from django.views.decorators.csrf import csrf_exempt
from CNC import models
from Crypto.Cipher import PKCS1_OAEP as PK
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import AES
from base64 import b64encode, b64decode
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


#@cache_page(CACHE_TTL)
@require_http_methods(["GET", "POST"])
@csrf_exempt
def commands(request):
    if request.method == "POST" and request.POST:
        cache.set("commands", json.dumps({x:y for x,y in list(request.POST.items())}))
        return HttpResponse(None)
    if request.method == "GET":
        commands = cache.get('commands')
        cache.delete("commands")
        if commands:
            return HttpResponse(b64encode(encrypt(commands)), content_type="text/plain")
        else:
            return HttpResponse({})


@require_http_methods(["GET", "POST"])
@csrf_exempt
def state(request):
    if request.method == "POST":
        cache.set("state", json.dumps({x: y for x, y in list(request.POST.items())}))
        return HttpResponse(None)
    if request.method == "GET":
        print(cache.get("state"))
        if cache.get("state"):
            return JsonResponse(json.loads(cache.get('state')))
        else:
            return HttpResponse(None)


@require_POST
@csrf_exempt
def persist_state(request):
    state = json.loads(cache.get("state"))
    if state:
        s = models.State(temperature=state["temperature"], humidity=state["humidity"], pitch=state["pitch"],
                         roll=state["roll"], yaw=state["yaw"], pixels=state["pixels"])
        s.save()
    return HttpResponse(None)


@require_http_methods(["POST"])
@csrf_exempt
def key(request):
    if request.method == "POST":
        public_key = request.body.decode()
        if not cache.get("aes_key"):
            aes_key = Random.get_random_bytes(16)
        else:
            aes_key = cache.get("aes_key")
        print(aes_key)
        encrypted_key = rsa_encrypt(aes_key, public_key)
        cache.set("aes_key", aes_key)
        return HttpResponse(b64encode(encrypted_key), content_type="text/plain")


def rsa_encrypt(msg, key_str):
    print(key_str)
    key = RSA.importKey(key_str)
    cipher = PK.new(key)
    return cipher.encrypt(msg)


def encrypt(msg):
    key = cache.get("aes_key")
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CFB, iv)
    return iv+cipher.encrypt(msg.encode())


def decrypt(msg):
    key = cache.get("aes_key")
    iv = msg[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CFB, iv)
    return cipher.decrypt(msg[AES.block_size])
