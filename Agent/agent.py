import requests
import json
from time import sleep
from sense_hat import SenseHat
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP as PK
from Crypto.Cipher import AES
from base64 import b64decode
from base64 import b64encode
from Crypto import Random


def rsa_decrypt(msg, key):
    cipher = PK.new(key)
    return cipher.decrypt(b64decode(msg))


def aes_encrypt(msg, key):
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CFB, iv)
    return iv+cipher.encrypt(msg.encode())


def aes_decrypt(msg, key):
    iv = msg[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CFB, iv)
    return cipher.decrypt(msg[AES.block_size:])


sense = SenseHat()
URL = "http://192.168.0.25:8000"
private_key = RSA.generate(2048)
response = requests.post(URL+"/key/", data=private_key.publickey().exportKey())
aes_key = rsa_decrypt(response.text, private_key)

stuff = '{"set_leds": "[[255, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]"}"'
print(aes_decrypt(aes_encrypt(stuff, aes_key), aes_key))
while True:
    commands = requests.get(URL+"/commands/").text
    if commands:
        print(commands)
        commands = aes_decrypt(b64decode(commands), aes_key)
        if "set_leds" in json.loads(commands):
            sense.set_pixels(json.loads(json.loads(commands)["set_leds"]))
    sensors = dict()
    sensors["humidity"] = sense.get_humidity()
    sensors["temperature"] = sense.get_temperature()
    sensors["pressure"] = sense.get_pressure()
    sensors["compass"] = sense.get_compass()
    sensors["led_array"] =str(sense.get_pixels())
    requests.post(URL+"/state/", data=sensors)
    sleep(10)


